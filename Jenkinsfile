import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import hudson.tasks.test.AbstractTestResultAction;
import hudson.model.Actionable;

node {

    def mvnHome
    
    environmentDashboard(
        addColumns: false
        , buildJob: 'artifactory'
        , buildNumber: '${BUILD_NUMBER}'
        , componentName: 'spring-clinic'
        , data: []
        , nameOfEnv: 'dev'
        , packageName: 'spring-clinic') {
    }
    
    properties([buildDiscarder(logRotator(artifactDaysToKeepStr: ''
        , artifactNumToKeepStr: ''
        , daysToKeepStr: '3'
        , numToKeepStr: '3'))])

    stage('Checkout code') { 
        cleanWs()
        // Clone code and checkout to branch
        // git branch: 'feature/jenkinsfile', url: 'https://github.com/mmehta-10/spring-petclinic.git'
        // checkout scm
        // checkout([
        //     $class: 'GitSCM'
        //     , branches: [[name: '**']]
        //     , extensions: [[$class: 'PathRestriction'
        //     , excludedRegions: 'deployment.jenkinsfile'
        //     , includedRegions: '']]
        //     , userRemoteConfigs: [[credentialsId: 'bitbucket', url: 'https://mmehta10@bitbucket.org/mmehta10/spring-pet-clinic.git']]
        //     ])

        checkout([
            $class: 'GitSCM'
            , branches: scm.branches
            , extensions: scm.extensions + [
                [$class: 'LocalBranch']
              , [$class: 'WipeWorkspace']
              , [$class: 'DisableRemotePoll']
              , [$class: 'PathRestriction'
                    , excludedRegions: '.*deployment.jenkinsfile.*'
                    , includedRegions: 'src/.*']
                ]
            , userRemoteConfigs: [[credentialsId: 'bitbucket', url: 'https://mmehta10@bitbucket.org/mmehta10/spring-pet-clinic.git']]
            , doGenerateSubmoduleConfigurations: false
        ])

        // Get the Maven tool.
        // ** NOTE: This 'M3' Maven tool must be configured in the global configuration.
        mvnHome = tool 'MAVEN_HOME'
    }
    
    // stage('Read POM') {
    //     script {
    //         def pom = readMavenPom file: 'pom.xml'            
    //         // Now you have access to raw version string in pom.version
    //         // Based on your versioning scheme, automatically calculate the next one            
    //         VERSION = pom.version
    //         echo "${VERSION}"
    //     }          
    // }
    
    stage('Build') {
        // Run the maven build
        withEnv(["MVN_HOME=$mvnHome"]) {
            if (isUnix()) {
                // sh '"$MVN_HOME/bin/mvn" -Dmaven.test.failure.ignore -Drevision="$BUILD_NUMBER" clean package'
                sh '"$MVN_HOME/bin/mvn" -Dmaven.test.skip=true -Drevision="$BUILD_NUMBER" clean package'
            } else {
                bat(/"%MVN_HOME%\bin\mvn" -Dmaven.test.failure.ignore -Drevision="$BUILD_NUMBER" clean package/)
            }
        }
        
        recordIssues tools: [java(), javaDoc()], aggregatingResults: 'true', id: 'java', name: 'Java'
    }
    
    stage('Unit tests - java') {
       try {
            withEnv(["MVN_HOME=$mvnHome"]) {
                if (isUnix()) {
                    sh '"$MVN_HOME/bin/mvn" -B test'
                } else {
                    bat(/"%MVN_HOME%\bin\mvn" -B test/)
                }
            }          
            
            junit testResults: '**/target/*-reports/*.xml'
            recordIssues tools: [
            //    checkStyle(),
                checkStyle(pattern: '**/checkstyle*.xml')
                ,spotBugs(pattern: 'target/spotbugsXml.xml')
                ,pmdParser(pattern: 'target/pmd.xml')
                ,cpd(pattern: 'target/cpd.xml')
                ,taskScanner(highTags:'FIXME', normalTags:'TODO', includePattern: '**/*.java', excludePattern: 'target/**/*')
            ]
            
            // step([$class: 'JUnitResultArchiver', testResults: '**/surefire-reports/*.xml', healthScaleFactor: 1.0])
            recordIssues(
                tools: [
                    junitParser(pattern: '**/surefire-reports/*.xml')
                    , findBugs(pattern: '**/findbugs*.xml', useRankAsPriority: true)
                ]
            )
            
            publishHTML (target: [
                    allowMissing: false,
                    alwaysLinkToLastBuild: false,
                    keepAll: true,
                    reportDir: 'target/surefire-reports',
                    reportFiles: '*.html',
                    reportName: "Junit Reports"
            ])
            
        } catch (err) {
            if (currentBuild.result == 'UNSTABLE')
                currentBuild.result = 'FAILURE'
            throw err
        } 
    }


//TODO
//Upload artifact only when branch == RELEASE
if(env.BRANCH_NAME ==~ /release_*([a-z0-9]*)/) {
    stage('Upload artifact'){

    try{
            if (env.CHANGE_ID) {
                echo("Looking for PR: PR detected, change id is ${env.CHANGE_ID}")
                def prBase = pullRequest.base
                if (prBase != 'main') {
                    currentBuild.result = 'ABORTED'
                    error("This PR is over ${prBase} branch, not 'main'. Aborting.")
                }
            }

            withEnv(["MVN_HOME=$mvnHome"]) {
                def server = Artifactory.server('private-jfrog')
                def rtMaven = Artifactory.newMavenBuild()
                rtMaven.tool = 'MAVEN_HOME'
                
                //  rtMaven.resolver server: server, releaseRepo: 'cpp-release-local', snapshotRepo: 'cpp-snapshot-local'
                rtMaven.deployer server: server, releaseRepo: 'cpp-release-local', snapshotRepo: 'cpp-snapshot-local'
                def buildInfo = rtMaven.run pom: 'pom.xml', goals: '-U package -Dmaven.test.skip=true -q -Dartifactory.publish.buildInfo=true -Drevision="$BUILD_NUMBER"'
                buildInfo = Artifactory.newBuildInfo()
                buildInfo.env.capture = true
                buildInfo.env.collect()
                //  buildInfo.retention maxBuilds: 1, maxDays: 7, doNotDiscardBuilds: ["3", "4"], deleteBuildArtifacts: true
                server.publishBuildInfo buildInfo
            }
        } catch(e){
            //if (currentBuild.result == 'UNSTABLE')
            currentBuild.result = 'FAILURE'
        } 
    }
}

    stage('Cleanup') {
        try{
            junit '**/target/surefire-reports/TEST-*.xml'
            // archiveArtifacts 'target/*.jar'
            // addShortText(text: 'text')
            //TODO
            def commit = sh(returnStdout: true, script: 'git rev-parse HEAD')            
            currentBuild.description = "defualt msg"
            currentBuild.description = "$commit"
        } catch(e){
            //if (currentBuild.result == 'UNSTABLE')
                currentBuild.result = 'FAILURE'
            throw e
        } finally{
            sh 'mvn clean'

            slackNotify()

            def currentResult = currentBuild.result ?: 'SUCCESS'
            if (currentResult == 'UNSTABLE') {
                echo 'This will run only if the run was marked as unstable'
            }

            def previousResult = currentBuild.getPreviousBuild()?.result
            if (previousResult != null && previousResult != currentResult) {
                echo 'This will run only if the state of the Pipeline has changed'
                echo 'For example, if the Pipeline was previously failing but is now successful'
            }

            echo 'This will always run'
        }
    }
}
    def slackNotify() {

        // Default values
        def colorName = 'RED'
        def colorCode = '#FF0000'
        def buildStatus = currentBuild.result == null ? "SUCCESS" : currentBuild.result
        def subject = "${buildStatus}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}] (<${env.RUN_DISPLAY_URL}|Open>) (<${env.RUN_CHANGES_DISPLAY_URL}|  Changes>)'"
        def title = "${env.JOB_NAME} Build: ${env.BUILD_NUMBER}"
        def title_link = "${env.RUN_DISPLAY_URL}"
        def branchName = "${env.BRANCH_NAME}"
        def commit = sh(returnStdout: true, script: 'git rev-parse HEAD')
        def author = sh(returnStdout: true, script: "git --no-pager show -s --format='%an'").trim()
        def message = sh(returnStdout: true, script: 'git log -1 --pretty=%B').trim() 

        // Override default values based on build status
        if (buildStatus == 'SUCCESS') {
            color = 'GREEN'
            colorCode = 'good'
        } else if (buildStatus == 'UNSTABLE') {
            color = 'YELLOW'
            colorCode = 'warning'
        } else {
            color = 'RED'
            colorCode = 'danger'
        }
        
        JSONObject attachment = new JSONObject();
        attachment.put('author',"jenkins");
        attachment.put('author_link',"https://danielschaaff.com");
        attachment.put('title', title.toString());
        attachment.put('title_link',title_link.toString());
        attachment.put('text', subject.toString());
        attachment.put('fallback', "fallback message");
        attachment.put('color',colorCode);
        attachment.put('mrkdwn_in', ["fields"])

        // JSONObject for branch
        JSONObject branch = new JSONObject();
        branch.put('title', 'Branch');
        branch.put('value', branchName.toString());
        branch.put('short', true);

        // JSONObject for author
        JSONObject commitAuthor = new JSONObject();
        commitAuthor.put('title', 'Author');
        commitAuthor.put('value', author.toString());
        commitAuthor.put('short', true);

        // JSONObject for commit
        JSONObject commitMessage = new JSONObject();
        commitMessage.put('title', 'Commit Message');
        commitMessage.put('value', message.toString());
        commitMessage.put('short', false);

        attachment.put('fields', [branch, commitAuthor, commitMessage]);
        JSONArray attachments = new JSONArray();
        attachments.add(attachment);
        println attachments.toString()

        if (buildStatus != 'SUCCESS') {
                
                slackSend botUser: true, 
                    channel: 'jenkins', 
                    color: '#00ff00', 
                    message: subject,
                    tokenCredentialId: 'slack-token',
                    // blocks: blocks    
                    attachments: attachments.toString()   
        }
    }